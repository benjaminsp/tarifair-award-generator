// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
var $ = require('jquery');

//get svg element.
var svg = document.getElementById("svg2");
var serializer = new XMLSerializer();
var source = serializer.serializeToString(svg);

if(!source.match(/^<svg[^>]+xmlns="http\:\/\/www\.w3\.org\/2000\/svg"/)){
    source = source.replace(/^<svg/, '<svg xmlns="http://www.w3.org/2000/svg"');
}
if(!source.match(/^<svg[^>]+"http\:\/\/www\.w3\.org\/1999\/xlink"/)){
    source = source.replace(/^<svg/, '<svg xmlns:xlink="http://www.w3.org/1999/xlink"');
}

source = '<?xml version="1.0" standalone="no"?>\r\n' + source;

var url = "data:image/svg+xml;charset=utf-8,"+encodeURIComponent(source);

document.getElementById("export_svg").href = url;




