<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Entity\Award;
use Symfony\Component\HttpFoundation\Request;

class AwardGeneratorController extends AbstractController
{
    /**
     * @Route("/", name="index")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {

        $award = new Award();
        $form = $this->createFormBuilder($award)
            ->add('productType', ChoiceType::class, array(
                'choices' => array(
                    'Privathaftpflicht' => 1,
                    'Hausrat' => 2,
                    'Wohngebäude' => 3,
                    'Unfall' => 4,
                    'Kfz' => 5,
                    'Rechtsschutz' => 6,
                    'Tierhalterhaftpflicht (Hund)' => 7,
                    'Tierhalterhaftpflicht (Pferd)' => 8,
                )
            ))
            ->add('company', TextType::class)
            ->add('logoSource', TextType::class)
            ->add('tariffName', TextType::class)
            ->add('tariffStand', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Generate'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $award = $form->getData();
            $maxScore = 0;

            $logoSource = $award->getLogoSource();
            $productType = $award->getProductType();
            switch ($productType) {
                case 1:
                    $productType = 'Private Haftpflicht';
                    $maxScore = 32;
                    break;
                case 2:
                    $productType = 'Hausrat';
                    $maxScore = 17;
                    break;
                case 3:
                    $productType = 'Wohngebäude';
                    $maxScore = 19;
                    break;
                case 4:
                    $productType = 'Unfall';
                    $maxScore = 16;
                    break;
                case 5:
                    $productType = 'Kfz';
                    $maxScore = 14;
                    break;
                case 6:
                    $productType = 'Rechtsschutz';
                    $maxScore = 13;
                    break;
                case 7:
                    $productType = 'Tierhalterhaftpflicht (Hund)';
                    $maxScore = 10;
                    break;
                case 8:
                    $productType = 'Tierhalterhaftpflicht (Pferd)';
                    $maxScore = 14;
                    break;
                default:
                    $productType = '';
            }
            $company = $award->getCompany();
            $tariffName = $award->getTariffName();
            $tariffStand = $award->gettariffStand();
            $month = date("m/Y");

            return $this->render('award_generator/index.html.twig', [
                'maxScore' => $maxScore,
                'productType' => $productType,
                'company' => $company,
                'logoSource' => $logoSource,
                'tariffName' => $tariffName,
                'tariffStand' => $tariffStand,
                'month' => $month,
            ]);
        }

        return $this->render('index.html.twig', [
            'form' => $form->createView(),
            'award' => $award,
        ]);
    }

    /**
     * @Route("/generator", name="award_generator")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function GeneratorAction()
    {
        return $this->render('award_generator/index.html.twig', [
            'controller_name' => 'AwardGeneratorController',
        ]);
    }
}
