<?php
/**
 * Created by PhpStorm.
 * User: benjamin
 * Date: 13.09.18
 * Time: 10:12
 */

// src/Entity/Task.php
namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Award {
    protected $maxScore;
    protected $company;
    protected $logoSource;
    protected $tariffName;
    protected $tariffStand;
    protected $productType;

    /**
     * @return mixed
     */
    public function getProductType()
    {
        return $this->productType;
    }

    /**
     * @param mixed $productType
     */
    public function setProductType($productType): void
    {
        $this->productType = $productType;
    }

    /**
     * @return mixed
     */
    public function getTariffName()
    {
        return $this->tariffName;
    }

    /**
     * @param mixed $tariffName
     */
    public function setTariffName($tariffName): void
    {
        $this->tariffName = $tariffName;
    }

    /**
     * @return mixed
     */
    public function getTariffStand()
    {
        return $this->tariffStand;
    }

    /**
     * @param mixed $tariffStand
     */
    public function setTariffStand($tariffStand): void
    {
        $this->tariffStand = $tariffStand;
    }

    /**
     * @return mixed
     */
    public function getMaxScore()
    {
        return $this->maxScore;
    }

    /**
     * @param mixed $maxScore
     */
    public function setMaxScore($maxScore): void
    {
        $this->maxScore = $maxScore;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company): void
    {
        $this->company = $company;
    }

    /**
     * @return mixed
     */
    public function getLogoSource()
    {
        return $this->logoSource;
    }

    /**
     * @param mixed $logoSource
     */
    public function setLogoSource($logoSource): void
    {
        $this->logoSource = $logoSource;
    }


}