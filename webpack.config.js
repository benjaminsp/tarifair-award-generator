var Encore = require('@symfony/webpack-encore');

Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')

    .addEntry('main', './assets/js/main.js')
    .addEntry('favicon', './assets/images/favicon.ico')
    .addStyleEntry('css/index', './assets/scss/main.scss')

    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())

    .enableSassLoader()

    // enables jQuery
    .autoProvidejQuery()

    .createSharedEntry('vendor', [
        'bootstrap',
    ])
;

module.exports = Encore.getWebpackConfig();
